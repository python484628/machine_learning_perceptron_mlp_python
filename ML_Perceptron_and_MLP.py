# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# January 2021


"""
**Perceptron with Python**

**1. scalling :**

We notice the important effect on the classifier.
"""

from sklearn.datasets import load_breast_cancer
cancer = load_breast_cancer()

from sklearn import preprocessing
scaled_X = preprocessing.minmax_scale(cancer.data)

from sklearn.linear_model import Perceptron
pc = Perceptron()

"""Perform a 5CV on the breast cancer dataset

For each classifier, 2 options to realize the CV :
- pc.fit = training / pc.predict = prediction
- or cross_val score

Warnings:simpletfilter('ignore') = allows to ignore warnings

"""

import warnings
warnings.simplefilter('ignore')

from sklearn.model_selection import cross_val_score
from sklearn import metrics

#Perform 5-fold cross validation
scores = cross_val_score(pc, cancer.data, cancer.target, cv=5)
print("Cross-validated scores:", scores)
print("Average 5CV score is %f +-%f(std)" %(scores.mean(), scores.std()))
# Perform 5-fold cross validation on scaled data
scores = cross_val_score(pc, scaled_X, cancer.target, cv=5)
print("Cross-Validated scores:", scores)
print("Average 5CV score is %f +-%f(std)" %(scores.mean(), scores.std()))

"""**Get the learned coefficients**

Allows to see which attribute is the most important so we can make a ranking

pc.coef : all coefficient's values in the attributes zip order : allows to send back coples between 2 lists (1,1 / 2,2...)

The number of epochs is limited by default to 100 for each iteration. Here CV is equal to 5 so 100 * 5. Therefore, for a NN (neuronal network), we should do a hold-out otherwise it's too costly.

"""

pc.fit(scaled_X, cancer.target)
print(pc)
print(list(zip(cancer.feature_names, pc.coef_[0])))

"""**Effect of the number of epochs**

We'll get the accuracy of perceptron with higher number of epochs

Epoch : it's a learning on the complete dataset.
We need some of them to learn correctly

One iteration is a passage on one data so there are several iteration for each epoch

Here :
- effect of the number of epoch on the classifier
- we stop at the 25th iteration because there is a merging at this iteration
"""

# Commented out IPython magic to ensure Python compatibility.
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
# %matplotlib inline

max_it = 200
max_it+=1
accuracy = []
for it in range(10,max_it,20):
  pc = Perceptron(max_iter=it)
  scores = cross_val_score(pc, scaled_X, cancer.target, cv=5)
  accuracy.append(scores.mean())
print(accuracy)
plt.figure(figsize=(20,5))
plt.plot(range(1,max_it,20), accuracy, label=" Perceptron Accuracy", marker='o', markerfacecolor='blue', color='skyblue', linewidth=4)
plt.legend()

from sklearn.datasets import load_breast_cancer
cancer = load_breast_cancer()

from sklearn import preprocessing
scaled_X = preprocessing.minmax_scale(cancer.data)

"""**Neural Networks : MLP**

Create the MLP classifier
"""

from sklearn.neural_network import MLPClassifier
mlp = MLPClassifier()

"""Perform a 5CV on the breast cancer dataset"""

# import warnings
# warnings.simplefilter('ignore')
# Command if needed

from sklearn.model_selection import cross_val_score
from sklearn import metrics

#Perform 5-fold cross validation
scores = cross_val_score(mlp, cancer.data, cancer.target, cv=5)
print("Cross-validated scores:", scores)
print("Average 5CV score is %f +-%f(std)" %(scores.mean(), scores.std()))
# Perform 5-fold cross validation
scores = cross_val_score(mlp, scaled_X, cancer.target, cv=5)
print("Cross-Validated scores:", scores)
print("Average 5CV score is %f +-%f(std)" %(scores.mean(), scores.std()))

"""**Effect of the number of hidden units**

We'll get the accuracy of MLP with different number of hidden units. We'll perform the experiment with three different number of hidden layers
"""

# Commented out IPython magic to ensure Python compatibility.
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
# %matplotlib inline

import warnings
warnings.simplefilter('ignore')

max_size = 200

accuracy, accuracy2, accuracy3 = [], [], []
for it in range(10,max_size+1, 20):
    mlp = MLPClassifier(hidden_layer_sizes=(max_size,1))
    scores = cross_val_score(mlp, scaled_X, cancer.target, cv=3)
    accuracy.append(scores.mean())
    mlp = MLPClassifier(hidden_layer_sizes=(max_size,2))
    scores = cross_val_score(mlp, scaled_X, cancer.target, cv=3)
    accuracy2.append(scores.mean())
    mlp = MLPClassifier(hidden_layer_sizes=(max_size,3))
    scores = cross_val_score(mlp, scaled_X,cancer.target, cv=3)
    accuracy3.append(scores.mean())

plt.figure(figsize=(20,5))
plt.plot(range(10,max_size, 20), accuracy, label="MLP Accuracy - 1 hidden layer", marker='o', color='skyblue', linewidth=4)
plt.plot(range(10,max_size, 20), accuracy2, label="MLP Accuracy - 2 hidden layers", marker='x', color='red', linewidth=4)
plt.plot(range(10,max_size, 20), accuracy3, label="MLP Accuracy - 3 hidden layers", marker='|', color='green', linewidth=4)
plt.legend()

"""**Effect of the activation function**

We'll get the accuracy of MLP with different activation function
"""

# Commented out IPython magic to ensure Python compatibility.
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
# %matplotlib inline

import warnings
warnings.simplefilter('ignore')

activation = ["logistic", "tanh", "relu"]
max_size = 100
max_size+=1
accuracies = []
for fn in activation:
    accuracy = []
    for it in range(10,max_size, 10):
        mlp = MLPClassifier(hidden_layer_sizes=(max_size,2), activation=fn)
        scores = cross_val_score(mlp, scaled_X, cancer.target, cv=3)
        accuracy.append(scores.mean())
    accuracies.append(accuracy)

plt.figure(figsize=(20,5))

from matplotlib.pyplot import cm
import numpy as np
colors=cm.rainbow(np.linspace(0,1,len(activation)))

for index, lst in enumerate(accuracies):
    plt.plot(range(10,max_size, 10), lst, label="MLP Accuracy - "+str(activation[index]), marker='o', color=colors[index], linewidth=2)
plt.legend()

# Commented out IPython magic to ensure Python compatibility.
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
# %matplotlib inline

import warnings
warnings.simplefilter('ignore')

max_size = 100
max_size+=1
max_nh1 = 5
max_nh1+=1
accuracies = []
for nh1 in range(1, max_nh1):
    accuracy = []
    for size in range(10,max_size, 10):
        mlp = MLPClassifier(hidden_layer_sizes=(size,nh1))
        scores = cross_val_score(mlp, scaled_X, cancer.target, cv=3)
        accuracy.append(scores.mean())
    accuracies.append(accuracy)

plt.figure(figsize=(20,5))

from matplotlib.pyplot import cm
import numpy as np
colors=cm.rainbow(np.linspace(0,1,max_nh1))

for index, lst in enumerate(accuracies):
    plt.plot(range(10,max_size,10), lst, label="MLP Accuracy - "+str(index+1)+"hidden layer", marker='o', color=colors[index], linewidth=2)
plt.legend()
