***Perceptron***

**Step 1**

Perform a 5CV on the breast cancer dataset.

**Step 2**

For each classifier, 2 options to realize the CV :
- pc.fit = training / pc.predict = prediction
- or cross_val score

**Step 3**

Get the learned coefficients.

**Step 4**
Effect of the number of epochs.


***Neural Networks : MLP***

**Step 1**

Create the MLP classifier.

**Step 2**

Perform a 5CV on the breast cancer dataset.

**Step 3**

Effect of the number of hidden units.

**Step 4**

Effect of the activation function.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

January 2021

